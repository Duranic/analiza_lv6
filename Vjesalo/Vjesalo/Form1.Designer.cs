﻿namespace Vjesalo
{
    partial class Vjesalo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input = new System.Windows.Forms.TextBox();
            this.label_unesi = new System.Windows.Forms.Label();
            this.check = new System.Windows.Forms.Button();
            this.izlaz = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.label_random = new System.Windows.Forms.Label();
            this.pobjeda_poraz_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // input
            // 
            this.input.Location = new System.Drawing.Point(124, 12);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(100, 20);
            this.input.TabIndex = 0;
            // 
            // label_unesi
            // 
            this.label_unesi.AutoSize = true;
            this.label_unesi.Location = new System.Drawing.Point(53, 15);
            this.label_unesi.Name = "label_unesi";
            this.label_unesi.Size = new System.Drawing.Size(65, 13);
            this.label_unesi.TabIndex = 1;
            this.label_unesi.Text = "Unesi slovo:";
            // 
            // check
            // 
            this.check.Location = new System.Drawing.Point(149, 38);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(75, 23);
            this.check.TabIndex = 2;
            this.check.Text = "Provjera";
            this.check.UseVisualStyleBackColor = true;
            this.check.Click += new System.EventHandler(this.check_Click);
            // 
            // izlaz
            // 
            this.izlaz.Location = new System.Drawing.Point(197, 226);
            this.izlaz.Name = "izlaz";
            this.izlaz.Size = new System.Drawing.Size(75, 23);
            this.izlaz.TabIndex = 3;
            this.izlaz.Text = "Izlaz";
            this.izlaz.UseVisualStyleBackColor = true;
            this.izlaz.Click += new System.EventHandler(this.izlaz_Click);
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(12, 226);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 4;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // label_random
            // 
            this.label_random.AutoSize = true;
            this.label_random.Location = new System.Drawing.Point(116, 226);
            this.label_random.Name = "label_random";
            this.label_random.Size = new System.Drawing.Size(42, 13);
            this.label_random.TabIndex = 5;
            this.label_random.Text = "random";
            // 
            // pobjeda_poraz_label
            // 
            this.pobjeda_poraz_label.AutoSize = true;
            this.pobjeda_poraz_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pobjeda_poraz_label.Location = new System.Drawing.Point(12, 116);
            this.pobjeda_poraz_label.Name = "pobjeda_poraz_label";
            this.pobjeda_poraz_label.Size = new System.Drawing.Size(221, 37);
            this.pobjeda_poraz_label.TabIndex = 6;
            this.pobjeda_poraz_label.Text = "pobjeda/poraz";
            this.pobjeda_poraz_label.Visible = false;
            // 
            // Vjesalo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.pobjeda_poraz_label);
            this.Controls.Add(this.label_random);
            this.Controls.Add(this.start);
            this.Controls.Add(this.izlaz);
            this.Controls.Add(this.check);
            this.Controls.Add(this.label_unesi);
            this.Controls.Add(this.input);
            this.Name = "Vjesalo";
            this.Text = "Vjesalo";
            this.Load += new System.EventHandler(this.Vjesalo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.Label label_unesi;
        private System.Windows.Forms.Button check;
        private System.Windows.Forms.Button izlaz;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label label_random;
        private System.Windows.Forms.Label pobjeda_poraz_label;
    }
}

