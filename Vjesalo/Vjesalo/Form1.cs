﻿//Napravite jednostavnu igru vješala.Pojmovi se učitavaju u listu iz datoteke, i u
//svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
//funkcionalnost koju biste očekivali od takve igre.Nije nužno crtati vješala,
//dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 

using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesalo
{
    public partial class Vjesalo : Form
    {
        public Vjesalo()
        {
            InitializeComponent();
        }
        //globalne stvari

        int pokusaj;

        char[] rastav;

        string random_pojam;

        int brojac = 0;

        class Pojmovi
        {
            private string pojam { get; set; }
            public Pojmovi() { pojam = "unknown"; }
            public Pojmovi(string novi_pojam) { pojam = novi_pojam; }
            public override string ToString()
            {
                return pojam;
            }
        }

        string put = String.Concat(Directory.GetCurrentDirectory(),"\\pojmovi.txt");
        List<Pojmovi> listapojmova = new List<Pojmovi> ();

        private void Vjesalo_Load(object sender, EventArgs e)
        {
            // Učitavanje pojmova iz daoteke u listu
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@put))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {   

                    string[] parts = line.Split('\n'); // razdvajanje linije na dijelove

                    Pojmovi P = new Pojmovi(parts[0]);

                    brojac++;

                    listapojmova.Add(P); // umetanje objekta u listu
                }
                
            }
        }

        private void izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void check_Click(object sender, EventArgs e)
        {
            bool empty = false;
            if (input.Text == "")
            {
                empty = true;
                MessageBox.Show("Polje za unos je prazno!");
            }
            if (!empty)
            {
                int noBrojac = rastav.Length;
                for (int i = 0; i < rastav.Length; i++)
                {
                    if (rastav[i].ToString() == input.Text && rastav[i] != ' ')
                    {
                        MessageBox.Show("Slovo se nalazi u trazenoj rijeci!");
                        break;
                    }
                    else if (input.Text == random_pojam)
                    {
                        pobjeda_poraz_label.Text = "Pobjeda";
                        pobjeda_poraz_label.Visible = true;
                        break;
                    }
                    else if (pokusaj <= 0)
                    {
                        pobjeda_poraz_label.Text = "Poraz";
                        pobjeda_poraz_label.Visible = true;
                        input.Visible = false;
                        break;
                    }
                    else
                    {
                        noBrojac--;
                        if (noBrojac == 0)
                        {
                            pokusaj--;
                            MessageBox.Show("Slovo se NE nalazi u trazenoj rijeci! Preostalo pokusaja: " + pokusaj.ToString());
                        }
                    }
                }
            }
        }

        private void start_Click(object sender, EventArgs e)
        {
            input.Visible = true;
            pobjeda_poraz_label.Visible = false;
            Random rnd = new Random();
            int random_broj = rnd.Next(1, brojac + 1);
            random_pojam = listapojmova[random_broj - 1].ToString();
            label_random.Text = random_pojam;
            rastav = random_pojam.ToCharArray();
            pokusaj = 6;
        }
    }
}
