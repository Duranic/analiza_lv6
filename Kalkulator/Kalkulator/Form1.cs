﻿//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
//znanstvenog kalkulatora, odnosno implementirati osnovne(+,-,*,/) i barem 5
//naprednih(sin, cos, log, sqrt...) operacija.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Kalkulator : Form
    {
        public Kalkulator()
        {
            InitializeComponent();
        }

        private void plus_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else rez.Text = (a + b).ToString();
        }

        private void minus_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else rez.Text = (a - b).ToString();
        }

        private void puta_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else rez.Text = (a * b).ToString();
        }

        private void kvadrat_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else rez.Text = (Math.Round((a * a),4)).ToString();
        }

        private void na_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else rez.Text = (Math.Round(Math.Pow(a, b),4)).ToString();
        }

        private void podijeljeno_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else if (b == 0)
            {
                MessageBox.Show("Nazivnik ne smije biti jednak nuli!", "Logicka greska");
            }
            else rez.Text = (Math.Round((a / b),2)).ToString();
        }

        private void korijen_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else if (a < 0)
            {
                MessageBox.Show("Uneseni broj je negativan!", "Logicka greska");
            }
            else rez.Text = (Math.Round(Math.Sqrt(a),4)).ToString();
        }

        private void e_na_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else rez.Text = (Math.Round(Math.Pow(2.72, a),4)).ToString();
        }

        private void log_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else if (a <= 0)
            {
                MessageBox.Show("Uneseni broj nije pozitivan!", "Logicka greska");
            }
            else rez.Text = (Math.Round(Math.Log(a),4)).ToString();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
