﻿namespace Kalkulator
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_a = new System.Windows.Forms.TextBox();
            this.textBox_b = new System.Windows.Forms.TextBox();
            this.label_a = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.podijeljeno = new System.Windows.Forms.Button();
            this.kvadrat = new System.Windows.Forms.Button();
            this.na = new System.Windows.Forms.Button();
            this.korijen = new System.Windows.Forms.Button();
            this.e_na = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.Button();
            this.label_rezultat = new System.Windows.Forms.Label();
            this.rez = new System.Windows.Forms.Label();
            this.exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_a
            // 
            this.textBox_a.Location = new System.Drawing.Point(36, 15);
            this.textBox_a.Name = "textBox_a";
            this.textBox_a.Size = new System.Drawing.Size(100, 20);
            this.textBox_a.TabIndex = 0;
            // 
            // textBox_b
            // 
            this.textBox_b.Location = new System.Drawing.Point(36, 47);
            this.textBox_b.Name = "textBox_b";
            this.textBox_b.Size = new System.Drawing.Size(100, 20);
            this.textBox_b.TabIndex = 1;
            // 
            // label_a
            // 
            this.label_a.AutoSize = true;
            this.label_a.Location = new System.Drawing.Point(12, 15);
            this.label_a.Name = "label_a";
            this.label_a.Size = new System.Drawing.Size(16, 13);
            this.label_a.TabIndex = 2;
            this.label_a.Text = "a:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "b:";
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(150, 15);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 23);
            this.plus.TabIndex = 4;
            this.plus.Text = "a + b";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(231, 15);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(75, 23);
            this.minus.TabIndex = 5;
            this.minus.Text = "a - b";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // puta
            // 
            this.puta.Location = new System.Drawing.Point(312, 15);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(75, 23);
            this.puta.TabIndex = 6;
            this.puta.Text = "a * b";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // podijeljeno
            // 
            this.podijeljeno.Location = new System.Drawing.Point(312, 44);
            this.podijeljeno.Name = "podijeljeno";
            this.podijeljeno.Size = new System.Drawing.Size(75, 23);
            this.podijeljeno.TabIndex = 7;
            this.podijeljeno.Text = "a / b";
            this.podijeljeno.UseVisualStyleBackColor = true;
            this.podijeljeno.Click += new System.EventHandler(this.podijeljeno_Click);
            // 
            // kvadrat
            // 
            this.kvadrat.Location = new System.Drawing.Point(150, 44);
            this.kvadrat.Name = "kvadrat";
            this.kvadrat.Size = new System.Drawing.Size(75, 23);
            this.kvadrat.TabIndex = 8;
            this.kvadrat.Text = "a ^ 2";
            this.kvadrat.UseVisualStyleBackColor = true;
            this.kvadrat.Click += new System.EventHandler(this.kvadrat_Click);
            // 
            // na
            // 
            this.na.Location = new System.Drawing.Point(231, 44);
            this.na.Name = "na";
            this.na.Size = new System.Drawing.Size(75, 23);
            this.na.TabIndex = 9;
            this.na.Text = "a ^ b";
            this.na.UseVisualStyleBackColor = true;
            this.na.Click += new System.EventHandler(this.na_Click);
            // 
            // korijen
            // 
            this.korijen.Location = new System.Drawing.Point(150, 73);
            this.korijen.Name = "korijen";
            this.korijen.Size = new System.Drawing.Size(75, 23);
            this.korijen.TabIndex = 10;
            this.korijen.Text = "√a";
            this.korijen.UseVisualStyleBackColor = true;
            this.korijen.Click += new System.EventHandler(this.korijen_Click);
            // 
            // e_na
            // 
            this.e_na.Location = new System.Drawing.Point(312, 73);
            this.e_na.Name = "e_na";
            this.e_na.Size = new System.Drawing.Size(75, 23);
            this.e_na.TabIndex = 11;
            this.e_na.Text = "e ^ a";
            this.e_na.UseVisualStyleBackColor = true;
            this.e_na.Click += new System.EventHandler(this.e_na_Click);
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(231, 73);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(75, 23);
            this.log.TabIndex = 12;
            this.log.Text = "log(a)";
            this.log.UseVisualStyleBackColor = true;
            this.log.Click += new System.EventHandler(this.log_Click);
            // 
            // label_rezultat
            // 
            this.label_rezultat.AutoSize = true;
            this.label_rezultat.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_rezultat.Location = new System.Drawing.Point(13, 96);
            this.label_rezultat.Name = "label_rezultat";
            this.label_rezultat.Size = new System.Drawing.Size(68, 21);
            this.label_rezultat.TabIndex = 13;
            this.label_rezultat.Text = "Rezultat: ";
            // 
            // rez
            // 
            this.rez.AutoSize = true;
            this.rez.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rez.Location = new System.Drawing.Point(87, 96);
            this.rez.Name = "rez";
            this.rez.Size = new System.Drawing.Size(0, 21);
            this.rez.TabIndex = 14;
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(312, 104);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(75, 23);
            this.exit.TabIndex = 15;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 139);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.rez);
            this.Controls.Add(this.label_rezultat);
            this.Controls.Add(this.log);
            this.Controls.Add(this.e_na);
            this.Controls.Add(this.korijen);
            this.Controls.Add(this.na);
            this.Controls.Add(this.kvadrat);
            this.Controls.Add(this.podijeljeno);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_a);
            this.Controls.Add(this.textBox_b);
            this.Controls.Add(this.textBox_a);
            this.Name = "Kalkulator";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_a;
        private System.Windows.Forms.TextBox textBox_b;
        private System.Windows.Forms.Label label_a;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button podijeljeno;
        private System.Windows.Forms.Button kvadrat;
        private System.Windows.Forms.Button na;
        private System.Windows.Forms.Button korijen;
        private System.Windows.Forms.Button e_na;
        private System.Windows.Forms.Button log;
        private System.Windows.Forms.Label label_rezultat;
        private System.Windows.Forms.Label rez;
        private System.Windows.Forms.Button exit;
    }
}

